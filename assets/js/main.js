$(document).ready(function() {
  // to hide error messages
  $('#name-error-message').hide();
  $('#email-error-message').hide();

  let errorName = false;
  let errorEmail = false;

  $('#name').focusout(function() {
    checkName();
  });
  $('#email').focusout(function() {
    checkEmail();
  });

  function checkName() {
    let namePattern = /^[a-zA-Z]*$/;  
    let name = $('#name').val();
    console.log(name);

    if (namePattern.test(name) && name !== '') {
      $('#name-error-message').hide();
      $('#name').css('border', '0.1rem solid mediumseagreen');
    } else {
      $('#name-error-message').html('Please should contain only characters');
      $('#name-error-message').css('position', 'relative');
      $('#name-error-message').css('top', '-3rem');
      $('#name-error-message').css('fontSize', '1.2rem');
      $('#name-error-message').css('color', 'red');
      $('#name-error-message').show();
      $('#name').css('border', '0.1rem solid red');
      errorName = true;
    }
  }

  function checkEmail() {
    let emailPattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/g;
    let email = $('#email').val();
    if (emailPattern.test(email) && email !== '') {
      $('#email-error-message').hide();
      $('#email').css('border', '0.1rem solid mediumseagreen');
    } else {
      $('#email-error-message').html('Invalid Email');
      $('#email-error-message').css('position', 'relative');
      $('#email-error-message').css('top', '-3rem');
      $('#email-error-message').css('fontSize', '1.2rem');
      $('#email-error-message').css('color', 'red');
      $('#email-error-message').show();
      $('#email').css('border', '0.1rem solid red');
      errorEmail = true;
    }
  }

  $('.form').submit(function() {
    errorName = false;
    errorEmail = false;

    checkName();
    checkEmail();

    if (!errorName && !errorEmail) {
      alert('Thank you for subscribing.');
      return true;
    } else {
      alert('Please fill the form correctly.');
      return false;
    }
  });
})